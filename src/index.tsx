import React from 'react'
import { View, Text } from 'react-native'

type Props = {
    name: string;
};

const Calendar: React.FunctionComponent<Props> = ({ name }) => (
    <View style={{ width: 200, height: 200, backgroundColor: 'red' }}>
        <Text style={{ color: '#fff' }}>Hello World! {name}</Text>
    </View>

)


function sayHello(name: string) {
    return `Hey ${name}, say hello to TypeScript.`;
}

export { Props, Calendar, sayHello }
export default Calendar