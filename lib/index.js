"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.sayHello = exports.Calendar = void 0;
var react_1 = __importDefault(require("react"));
var react_native_1 = require("react-native");
var Calendar = function (_a) {
    var name = _a.name;
    return (react_1.default.createElement(react_native_1.View, { style: { width: 200, height: 200, backgroundColor: 'red' } },
        react_1.default.createElement(react_native_1.Text, { style: { color: '#fff' } },
            "Hello World! ",
            name)));
};
exports.Calendar = Calendar;
function sayHello(name) {
    return "Hey " + name + ", say hello to TypeScript.";
}
exports.sayHello = sayHello;
exports.default = Calendar;
